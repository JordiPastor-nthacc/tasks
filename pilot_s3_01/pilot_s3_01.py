# !/usr/bin/python3
# -*- coding: utf-8 -*-
"""
to do in this version: add interactive behavior when >85% perf (18 out of 20) [real deal: how to update vector choice ~ rachid's trend plugin working fine with it?]
 - Cube matrix pending, is it really worth?
"""
import user_settings as conf # to use vars: conf.VAR_0
from pybpodapi.bpod import Bpod
from pybpodapi.state_machine import StateMachine
# from pybpodapi.protocol import Bpod, StateMachine
from pybpodapi.bpod.hardware.events import EventName
from pybpodapi.bpod.hardware.output_channels import OutputChannel
#from pysettings import conf
import timeit, random, numpy as np, time, datetime
from toolsR import SoundR, VideoR # DataR not req anymore
import os
from scipy.signal import firwin, lfilter
import sounddevice as sd

currSessionVid = os.path.expanduser('~/VIDEO_pybpod/'+conf.VAR_BOX+'/')
currSessionName = 'test_'+datetime.datetime.now().strftime('%Y%m%d_%H%M') # in a future replace test for the name R33_pprotocolname_+ others | would be good that this matches session timestamp

if not os.path.exists(os.path.expanduser(currSessionVid)):
    os.makedirs(os.path.expanduser(currSessionVid))

nocam = False
try:
    cam = VideoR(int(conf.VAR_CAMIDX), path= currSessionVid) #width='default', height='default',
    camOK=False                      #  fps=60)#, fourcc_mjpg='default', showWindows=True)
    cam.play()
    if int(conf.VAR_REC)>0:
        cam.record()
except:
    print("could not open device. This may happen because either it's already in use or wrong device index number was provided")
    nocam = True

def getWaterCalib(box):
    '''returns tupple (L,C,R) valvetime for a given box (boardname inside .npy calib-file is req to match). Still dont know what happens if non-existen str is used as arg'''
    watervalues = np.load(os.path.expanduser('~/pluginsr-for-pybpod/water-calibration-plugin/DATA/water_calibration_hystory.npy')).item()
    idx = next(i for i,v in zip(range(len(list(watervalues['board']))-1, -1, -1), reversed(list(watervalues['board']))) if v == box)
    return watervalues['results'][0][idx], watervalues['results'][1][idx], watervalues['results'][2][idx]

def getBroadbandCalib(xonaridx):
    '''returns tupple (L,R) of amp [latter calib values] of a given soundcard index (same as SoundR.getDevices())'''
    broadband = np.load(os.path.expanduser('~/pluginsr-for-pybpod/sound-calibration-plugin/DATA/broadbandnoise_hystory.npy')).item()
    Lidx = next(i for i,v,w in zip(range(len(list(broadband['Bsoundcard']))-1, -1, -1), reversed(list(broadband['Bsoundcard'])), reversed(list(broadband['Bside']))) if v.startswith(str(xonaridx)) and w=='L')
    Ridx = next(i for i,v,w in zip(range(len(list(broadband['Bsoundcard']))-1, -1, -1), reversed(list(broadband['Bsoundcard'])), reversed(list(broadband['Bside']))) if v.startswith(str(xonaridx)) and w=='R')
    return (broadband['BAmp'][Lidx], broadband['BAmp'][Ridx])

try:# find last water calibration
    ValveLtime, ValveCtime, ValveRtime = getWaterCalib(conf.VAR_BOX)
    print('valve timings (L, R):')
    print(ValveLtime, ValveRtime)
except:
    print('did not find water calib file. loading [0.12]*3')
    ValveLtime, ValveCtime, ValveRtime = (0.12, 0.12, 0.12)

try:# same but broadband
    LAmp, RAmp = getBroadbandCalib(int(conf.VAR_XONAR))
    print('Amp values (L,R):'+str(LAmp)+' '+str(RAmp))
except:
    print('did not find broadband calib file. loading amplitudes (0.5, 0.5)')
    LAmp, RAmp = (0.5, 0.5)


# session parameters [need to group them altogether] at the start of the script, when possible
LEDINT = 8 # from 0 to 255
LEDL, LEDC, LEDR = (OutputChannel.PWM1,LEDINT), (OutputChannel.PWM2,LEDINT), (OutputChannel.PWM3,LEDINT)
nTrials = 3000
sessionTimeLength = 5400


coin = random.choice(['heads', 'tails']) # from which side should be the first block
trial_list = []
if int(conf.VAR_BNUM)!=0:
    L = [0]*int(conf.VAR_BLEN) 
    R = [1]*int(conf.VAR_BLEN)
    if coin == 'heads':
        trial_list = (L+R)*(int(conf.VAR_BNUM)/2)
    else:
        trial_list = (R+L)*(int(conf.VAR_BNUM)/2)


pending_trials = np.random.choice([0,1], nTrials - len(trial_list)).tolist()
trial_list = trial_list + pending_trials
switchedToRandom = False

sessionTimeStart = time.time()
sessionTimeEnding = sessionTimeStart+sessionTimeLength

# Generation of white noise.
mean = 0
std = 1
duration = 1
#amplitude = 0.5 # need to import calib and do it for both sides | by hand till new version // creating 2 new vars: VAR_AMPL and VAR_AMPL
FsOut = 192000  # sample rate, depend on the sound card
band_fs = [500, 20000]
white_noise1 = 1 * np.random.normal(mean, std, size=FsOut * (duration + 1)) # 1 second more that will be removed at the end
# Filter the white noise using a pass-band
Fn = 10000 # Lenght of the filter ohoheoeo this will take some time
band_pass = firwin(Fn, [band_fs[0]/(FsOut*0.5), band_fs[1]/(FsOut*0.5)], pass_zero=False) # Pass-band fileter
band_noise1 = lfilter(band_pass, 1, white_noise1) # Filtering
s1 = band_noise1[FsOut:FsOut*(duration+1)] # remove the first second: it has some zeros casused by the phase of the filter LEFT

# Create the sound server 
soundStream = SoundR(sampleRate=FsOut, deviceOut=int(conf.VAR_XONAR), channelsOut=2) #change device depending on the sound card

# define evidence stages and when to switch
evidences = {
    'e1':(1, 0.866, -0.866, -1), # replacing abs 0.9 by .866 (impossible val with variance=0.06)
    'e2':(1, 0.866, 0.8, -0.8,-0.866, -1),
    'e3':(1, 0.8, 0.5, -0.5,-0.8, -1),
    'e4':(1, 0.75, 0.45, -0.45, -0.75, -1),
    'e5':(0.866, 0.5, 0.3, 0.15, -0.15, -0.3, -0.5, -0.866)
}
e_thr = {
    'e1': (0, .95),
    'e2': (.85, .90),
    'e3': (.80, .85),
    'e4': (.75, .80),
    'e5': (.70, 1)
}

#curr_evidences = 'e1' from now on, var e and use list(evidences.keys())[e]
e=0
perfwindow = [0]*25 # to eval whether they are performing good

def my_softcode_handler(data):
    global start, timeit, soundStream, cam
    print(data)
    if data == 68:
        print("Play")
        soundStream.playSound()
        start = timeit.default_timer()
    elif data == 66:
        soundStream.stopSound()
        print(':::::::::Time to stop:::::::::', timeit.default_timer() - start)
        

START_APP = timeit.default_timer()
def evidencetocoh(x):
    return (x+1)/2
def cohtoevidence(x):
    return 2*x+1 # incomplete, still lacks if and perhaps -1 instead
# we will define a function so it gets evaluated in each trial. will it? :)
def remainingTime():
    return sessionTimeEnding-time.time()

def envelope(coh, whitenoise, dur, nframes, samplingR, variance, randomized=False, paired=True):
  '''coh: coherence from 0(left only)to 1(right). ! var < coh < (1-var). Else this wont work
  whitenoise: vec containing sound (not necessarily whitenoise)
  dur: total duration of the stimulus (secs)
  nframes: total frames in the whole stimulus
  samplingR: soundcard sampling rate (ie 96000). Need to match with EVERYTHING
  variance: fixed var
  randomized: shuffles noise vec
  paired: each instantaneous evidence is paired with its counterpart so their sum = 1
  returns: left noise vec, right noise vec, left coh stairs, right coh stairs [being them all 1d-arrays] 
  '''
  totpoints= dur*samplingR # should be an integer
  if len(whitenoise)<totpoints:
    return 'whitenoise is shorter than expected', 0, 0, 0
  if randomized==True:
    svec = whitenoise[:int(totpoints)]
    svec = svec.reshape(int(len(svec)/10),10)
    np.random.shuffle(svec)
    svec = svec.flatten()
  else:
    svec = whitenoise[:int(totpoints)]


  modfreq = nframes/dur
  modwave = 1*np.sin(2*np.pi*(modfreq)*np.arange(0, dur, step=1/samplingR)+np.pi)

  
  if coh<0 or coh>1:
    raise ValueError('invalid coh (<0 | >1)')
    return "invalid coherence, need (0~1)", 0, 0, 0
  elif coh==0 or coh==1:
    staircaseR = np.repeat(coh, dur*samplingR)
    staircaseL = staircaseR-1
    Lout = staircaseL*svec*modwave*LAmp
    Rout = staircaseR*svec*modwave*RAmp
    return Lout, Rout, np.repeat(coh-1,nframes), np.repeat(coh, nframes)
  elif coh<=(variance*1.1) or coh>=1-variance*1.1:
    raise ValueError('invalid coherence for given variance or viceversa (if coh!=0|1, 1.1*var<coh<1-var*1.1)')
  else:
    alpha = ((1-coh)/variance -1/coh)*coh**2
    beta = alpha*(1/coh-1)
    stairs_envelopeR = np.random.beta(alpha,beta, size=nframes)
    staircaseR = np.repeat(stairs_envelopeR, int(totpoints/nframes))
    staircaseL = staircaseR-1
    Rout=staircaseR*svec*modwave*RAmp
    if paired==False:
      stairs_envelopeL = np.random.beta(alpha,beta,size=nframes)-1
      staircaseL=np.repeat(stairs_envelopeL, int(totpoints/nframes))
      Lout=staircaseL*svec*modwave*LAmp
      return Lout, Rout, stairs_envelopeL, stairs_envelopeR
    Lout = staircaseL*svec*modwave*LAmp
    return Lout, Rout, stairs_envelopeR-1, stairs_envelopeR


my_bpod = Bpod() 
my_bpod.register_value('VECTOR_CHOICE', trial_list) # required for trend plugin [0-left, 1-right]
my_bpod.register_value('left_amp', LAmp)
my_bpod.register_value('right_amp', RAmp)
my_bpod.register_value('left_valve', ValveLtime)
my_bpod.register_value('right_valve', ValveRtime)
# here, try to include this statement inside the loop so we can gain interactivity (if it does not lag :D)
my_bpod.softcode_handler_function = my_softcode_handler

while True:
    for i in range(0,len(trial_list),1):  # Main loop
        coh_to_chose_from = np.array(evidences[list(evidences.keys())[e]])
        

        if trial_list[i] == 0:
            curr_coh = evidencetocoh(np.random.choice(coh_to_chose_from[coh_to_chose_from<=0]))
            valvetime = ValveLtime
            rewardValve = (OutputChannel.Valve, 1)
            pokechange = {EventName.Port1In:'Reward', EventName.Port3In:'Punish'}#EventName.Tup:  goIdle}
            rewardLED = LEDL
        elif trial_list[i] == 1:
            curr_coh = evidencetocoh(np.random.choice(coh_to_chose_from[coh_to_chose_from>=0]))
            valvetime = ValveRtime
            rewardValve = (OutputChannel.Valve, 3)
            pokechange = {EventName.Port3In:'Reward', EventName.Port1In:'Punish'}#EventName.Tup: goIdle}
            rewardLED = LEDR
        # time this,
        SL,SR, EL, ER = envelope(curr_coh, s1, 1, 20, FsOut, 0.06, randomized=False, paired=False)
        soundStream.load(SL, SR)
        my_bpod.register_value('coherence01', curr_coh)
        my_bpod.register_value('left_envelope', EL)
        my_bpod.register_value('right_envelope', ER)

        sma = StateMachine(my_bpod)
        sma.set_global_timer_legacy(timer_id=1, timer_duration=remainingTime())
        
        sma.add_state(#WaitCPoke
            state_name='WaitCPoke',
            state_timer=1,
            state_change_conditions={Bpod.Events.Port2In:'Fixation', Bpod.Events.GlobalTimer1_End: 'exit'},
            output_actions=[LEDC,(Bpod.OutputChannels.GlobalTimerTrig, 1)])
        sma.add_state(#Fixation
            state_name='Fixation',
            state_timer=0.3,
            state_change_conditions={Bpod.Events.Port2Out:'WaitCPoke', EventName.Tup:'StartSound'},
            output_actions=[LEDC])
        sma.add_state(#StartSound
            state_name='StartSound',
            state_timer=0.2,
            state_change_conditions=pokechange,
            output_actions=[(OutputChannel.SoftCode, 68)])#,(Bpod.OutputChannels.GlobalTimerTrig, 1)])
        sma.add_state(#Reward
            state_name='Reward',
            state_timer=valvetime,
            state_change_conditions={EventName.Tup: 'keep-led-on'},
            output_actions=[(OutputChannel.SoftCode, 66), rewardValve, rewardLED])
        sma.add_state(#keep-led-on
            state_name='keep-led-on',
            state_timer=0.3-valvetime,
            state_change_conditions= {EventName.Tup: 'exit'},
            output_actions=[rewardLED])
        sma.add_state(#Punish
            state_name='Punish',
            state_timer=2,
            state_change_conditions= {EventName.Tup: 'exit'},
            output_actions=[(OutputChannel.SoftCode, 66)])

        my_bpod.send_state_machine(sma)  # Send state machine description to Bpod device

        print("Waiting for poke. Reward: ", 'left' if trial_list[i] == 0 else 'right')

        my_bpod.run_state_machine(sma)  # Run state machine

        trialstring = ("Current trial info: {0}".format(my_bpod.session.current_trial))


        if "'Punish': [(nan, nan)]" in trialstring:# Punish state was not triggered = rat poked in the correct port. This one is enough since it is binary, modify when introdicing invalid trials
            perfwindow = perfwindow[1:]+[1]
        else:
            perfwindow = perfwindow[1:]+[0]

        if i%5==0: # gets evaluated one out of 5
          if sum(perfwindow)/25>e_thr[list(e_thr.keys())[e]][1]:# incr. diff
            if e==len(evidences.keys())-1:
              e=e # already hardest
            else:
              e+=1
          elif sum(perfwindow)/25<e_thr[list(e_thr.keys())[e]][0]: # decr. diff
            if e==0:
              e=e # already easiest
            else:
              e-=1 #decreases dif


        if time.time()>sessionTimeEnding:
            break
    if time.time()>sessionTimeEnding:
        break
my_bpod.close()  # Disconnect Bpod and perform post-run actions

if nocam==False:
    cam.stop()

print('EXECUTION TIME', timeit.default_timer() - START_APP)