import numpy as np
import matplotlib.pyplot as plt
import glob, os, sys

import csv

from scipy import stats

from scipy.optimize import minimize

from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.lines import Line2D

from pathlib import Path

import pandas as pd

import logging
import warnings

#warnings.simplefilter("error") # Used so that numpy warnings raise exceptions

logging.basicConfig(level=logging.INFO)
#logging.basicConfig(level=logging.DEBUG) # Uncomment this line if you want to see the script's progress in the terminal

logging.info("Starting daily report...")

try:
    df = pd.read_csv(glob.glob("*.csv")[0], skiprows=6, sep=';') # Uncomment this line for manual editing of CSV files
    #df = pd.read_csv('data.csv', skiprows=6, sep=';')
except FileNotFoundError:
    logging.critical("CSV file not found. Exiting...")
    sys.exit(1)

logging.debug("CSV file found. Continuing...")

results             = [] # Contains True if the answer was correct, False otherwise
reward_side         = [] # Contains 1 if the correct answer was the RIGHT side, 0 otherwise
trial_num           = 0
correct_trials      = 0
correct_trials_per_cent = 0 
valid_trials        = 0
valid_trials_per_cent = 0
subject_name        = ""
setup_name          = ""
date                = ""
length              = 0  # Length of the record (number of trials)
session_name        = ""
left_indices        = [] # Contains the indices (1,2,3...) for the LEFT side trials
left_performance    = [] # Contains the performance of the LEFT side channel as a rolling window of 20 samples
right_indices       = [] # Contains the indices (1,2,3...) for the RIGHT side trials
right_performance   = [] # Contains the performance of the RIGHT side channel as a rolling window of 20 samples
binary_perf         = [] # Contains 1 for a correct trial, 0 otherwise
invalids            = [] # Contains False in those indices that correspond to an invalid trial, True otherwise
invalid_flag        = 0  # This flag is 1 if the trial doesn't have invalid trials

def compute_window(data):
    performance = []
    for i, elem in enumerate(data):
        if i < 20: 
            performance.append(round(np.mean(data[0:i+1]), 2))
        else:
            performance.append(round(np.mean(data[i-20:i]), 2))
    return performance

def compute_channel_performance(reward_side, results, invalids):

    perf_indices = []
    binary_perf = []
    left_indices = []
    left_trials = []
    right_indices = []
    right_trials = []
    right_performance = []
    left_performance = []
    performance = []

    for i, elem in enumerate(reward_side):
        if invalids[i] == True:
            perf_indices.append(i+1)
            binary_perf.append(results[i])
            if elem == 0: 
                left_indices.append(i+1)
                left_trials.append(results[i])
            else: 
                right_indices.append(i+1)
                right_trials.append(results[i])

    if left_trials:
        left_performance = compute_window(left_trials)
        total_L_performance = round(np.mean(left_trials), 2)
    else:
        left_indices = [0]
        left_trials = [0]
        left_performance = [0]
        total_L_performance = 0

    if right_trials:
        right_performance = compute_window(right_trials)
        total_R_performance = round(np.mean(right_trials), 2)
    else:
        right_indices = [0]
        right_trials = [0]
        right_performance = [0]
        total_R_performance = 0

    performance = compute_window(binary_perf)

    return binary_perf, perf_indices, performance, left_indices, right_indices, left_performance, right_performance, total_R_performance, total_L_performance

##############################
# Gather session metadata:
##############################

try:
    setup_name = df[df.MSG == 'SETUP-NAME']['+INFO'].iloc[0]
except IndexError:
    setup_name = "Unknown box"
    logging.warning("Box name not found.")
    
try:
    session_name = df[df.MSG == 'SESSION-NAME']['+INFO'].iloc[0]
except IndexError:
    session_name = "Unknown session"
    logging.warning("Session name not found.")

try:
    subject_name = df[df.MSG == 'SUBJECT-NAME']['+INFO'].iloc[0]
    subject_name = subject_name[1:-1].split(',')[0]
except IndexError:
    subject_name = "Unknown subject"
    logging.warning("Subject name not found.")
    
try:
    session_started = df[df.MSG == 'SESSION-STARTED']['+INFO'].iloc[0]
    date = session_started.split()
    time = date[1][0:8]
    day = date[0]
except IndexError:
    session_started = "??"
    time = "??"
    day = "??"
    logging.warning("Session start time not found.")

logging.debug("Session metadata loaded. Continuing...")

##############################
# Manage directories:
##############################

if not os.path.exists(os.path.expanduser("~/daily_reports/")): 
    os.makedirs(os.path.expanduser("~/daily_reports/"))
    logging.info("Daily_report directory not found. Creating it...")
if not os.path.exists(os.path.expanduser("~/daily_reports/"+subject_name)): 
    os.makedirs(os.path.expanduser("~/daily_reports/"+subject_name))
    logging.info("Directory for this subject not found. Creating it...")
os.chdir(os.path.expanduser("~/daily_reports/"+subject_name))

logging.debug("Created/accessed the directories. Continuing...")

##############################
# Gather experiment results:
##############################

# length is necessary because sometimes the csv file ends with a trial where all states are Nan:
length = np.sum(df[df['MSG'] == 'StartSound']['BPOD-FINAL-TIME'].apply(lambda x: not np.isnan(x)).values)
# results contains True if the answer was correct, False otherwise:
results = df[df['MSG'] == 'Punish']['BPOD-FINAL-TIME'].apply(lambda x: np.isnan(x))[:length]
# invalids contains False for invalid trials and True otherwise
invalids = df[df['MSG'] == 'Invalid']['BPOD-FINAL-TIME'].apply(lambda x: np.isnan(x)).values.tolist()[:length]
if not invalids:
    invalids = [True] * length
    invalid_flag = 1
    logging.warning("This session didn't take invalid trials into account.")
# reward_side contains a 1 if the correct answer was the Right side, 0 otherwise:
try:
    reward_side = list(map(int, df[df.MSG=='REWARD_SIDE']['+INFO'].iloc[0][1:-1].split(',')))[:length]
except IndexError: # Compatibility with old files
    logging.warning("REWARD_SIDE vector not found. Trying old VECTOR_CHOICE...")
    try:
        reward_side = list(map(int, df[df.MSG=='VECTOR_CHOICE']['+INFO'].iloc[0][1:-1].split(',')))[:length]
    except IndexError:
        logging.critical("Neither REWARD_SIDE nor VECTOR_CHOICE found. Exiting...")
        sys.exit(1)
# mean of the response times:
response_time = df[df.MSG=='WaitResponse']['+INFO'].values[:length]
try:
    response_time = int(1000 * np.mean(list(map(float, response_time.tolist()))))
except:
    response_time = 0
    logging.warning("No response time found; using 0 ms from now on.")

# Compute the performances:
binary_perf, perf_indices, performance, left_indices, right_indices, left_performance, right_performance, total_R_performance, total_L_performance = compute_channel_performance(reward_side, results.tolist(), invalids)

# total number of trials, number of valid and correct trials, and percentage:
trial_num = length
invalid_trials = trial_num - len(performance)
correct_trials = binary_perf.count(1)
correct_trials_per_cent = round(correct_trials * 100 / trial_num, 2)
if invalid_flag:
    invalid_trials = 0
    invalid_trials_text = "Not taken into account"
else:
    invalid_trials = trial_num - len(performance)
    invalid_trials_text = str(invalid_trials)
invalid_trials_per_cent = round(invalid_trials * 100 / trial_num, 2)

logging.debug("Experiment results gathered. Continuing...")

##############################
# Compute the psychometric curve:
##############################

def R_resp(reward_side, results):
    r_resp = []
    for i, elem in enumerate(results):
        if reward_side[i] == elem: r_resp.append(1)
        else: r_resp.append(0)
    return r_resp
R_resp = R_resp(reward_side, results)

coherences =  list(map(float, (df[df['MSG'] == 'coherence01']['+INFO'].values[:length])))
evidences = list(map(lambda x: round(2*x-1, 1), coherences))
a = {'R_resp': R_resp, 'evidence': evidences, 'coh': coherences}
coherence_dataframe = pd.DataFrame(a)

info = coherence_dataframe.groupby(['evidence'])['R_resp'].mean()
ydata = info.values
xdata = info.index.values

# compute MLE fit:

def sigmoid_MME(params):
    k = params[0]
    x0 = params[1]   
    B = params[2]
    P = params[3]

    yPred = B+(1-B-P)/(1 + np.exp(-k*(xdata-x0)))

    # Calculate negative log likelihood
    LL = -np.sum( stats.norm.logpdf(ydata, loc=yPred) )

    return(LL)

err = coherence_dataframe.groupby(['coh'])['R_resp'].sem()
LL = minimize(sigmoid_MME, [1,1,0,0])

k = LL['x'][0]
x0 = LL['x'][1]
B = LL['x'][2]
P = LL['x'][3]

fit = B+(1-B-P)/(1 + np.exp(-k*(np.linspace(-1,1,30)-x0)))

logging.debug("Psychometric curve computed. Continuing...")

##############################
# Create daily report with performance and psychometric plots:
##############################

with PdfPages(session_name + '.pdf') as pdf:
        plt.figure(figsize=(8, 4.5))
        axes1 = plt.subplot(111)
        axes1.set_ylim([0,1.1])
        axes1.set_yticks(list(np.arange(0,1.1, 0.1)))
        axes1.set_yticklabels(['0', '', '','','','50', '','','','','100'])
        axes1.grid(linestyle = 'dashed', alpha = 0.6)
        axes1.set_title("Accuracy")
        axes1.plot(perf_indices, performance, marker = 'o', markersize=2, color = 'black', linewidth = 0.7)
        axes1.plot(left_indices, left_performance, marker = 'o', markersize=2, color = 'cyan', linewidth = 0.7)
        axes1.plot(right_indices, right_performance, marker = 'o', markersize=2, color = 'magenta', linewidth = 0.7)
        axes1.set_xlim([1,length+1])
        axes1.set_ylabel('Accuracy [%]')
        axes1.set_xlabel('Trials')
        legend_elements = [Line2D([0], [0],color='w', marker='o',markersize=5, markerfacecolor="white", markeredgecolor = "black", label='Total performance'),
        Line2D([0], [0], marker='o', color='w', markersize=5, markerfacecolor="white", markeredgecolor = "cyan", label='Left channel'),
        Line2D([0], [0], marker='o', color='w',markersize=5, markerfacecolor="white", markeredgecolor = "magenta", label='Right channel')]
        plt.legend(bbox_to_anchor=(1,0), loc="lower right", bbox_transform= plt.gcf().transFigure, handles=legend_elements, ncol=3)
        plt.text(0.02, 0.85, "Date: " + day + " " + time + "\n" + "Subject name: " + str(subject_name) + " / " + "Box: " + setup_name + "\n" + 
        "Total trials: " + str(trial_num) + " / " + "Correct trials: " + str(correct_trials) + " (" + str(correct_trials_per_cent) + "%)" + 
        " / " + "Invalid trials: " + invalid_trials_text + " (" + str(invalid_trials_per_cent) + "%)\n" +
        "Water: " + str(round(correct_trials*0.024,3)) + " mL"+ " / " + "Mean response time: " + str(response_time) + " ms", fontsize=8, transform=plt.gcf().transFigure)
        plt.subplots_adjust(bottom=0.2, top = 0.75)

        pdf.savefig()  # saves the current figure into a pdf page
        plt.close() 

        plt.plot([0,0], [0, 1], 'k-', lw=1, linestyle=':')
        plt.plot([-1, 1], [0.5, 0.5], 'k-', lw=1, linestyle=':')
        plt.errorbar(xdata, ydata, yerr=err.values, fmt='ro', elinewidth = 1, markersize = 3)
        plt.plot(np.linspace(-1,1,30), fit, color = 'black', linewidth = 1)
        plt.yticks(np.arange(0, 1.1, step=0.1))
        plt.grid(linestyle = 'dashed', alpha = 0.7)
        plt.title("Psychometric curve")
        plt.xlabel('evidence')
        plt.ylabel('R-prob')
        plt.xlim([-1.05, 1.05])
        plt.ylim([-0.05,1.05])
        plt.annotate(str(round(ydata[0],2)), xy=(xdata[0], ydata[0]), xytext=(xdata[0]-0.03, ydata[0]+0.05), fontsize = 8)
        plt.annotate(str(round(ydata[-1],2)), xy=(xdata[-1], ydata[-1]), xytext=(xdata[-1]-0.03, ydata[-1]-0.08), fontsize = 8)
        plt.annotate("k= " + str(round(k,2)) + "\n" + "x0= " + str(round(x0,2))+ "\n" + "B= " + str(round(B,2))+ "\n" +"P= " + str(round(P,2)), xy =(0,0), xytext = (-0.96,0.85), fontsize = 8 )
        pdf.savefig(plt.gcf())  # saves the current figure into a pdf page
        plt.close() 

logging.debug("Daily report done. Continuing...")
        
##############################
# Write data to hidden file and prepare the inter-session report:
##############################

R_performance_record = []
L_performance_record = []
performance_record = []
correct_trial_record = []
invalid_trial_record = []
psych_x_record = []
psych_y_record = []
psych_fit_record = []
psych_err_record = []
dates_record = []
mean_times_record = []

xdata = xdata.tolist()
ydata = np.round(ydata, 3).tolist()
error = np.round(err.values, 3).tolist()
fit = np.round(fit, 3).tolist()

file_path = Path(".session_data.csv")
if not file_path.exists():
    file = open(".session_data.csv", "w", newline='')
    writer = csv.writer(file, delimiter = ';')
    # If the files doesn't exist yet, create a new CSV file with the following header:
    writer.writerow(("SESSION-NUMBER","TRIALS","CORRECT_TRIALS", "INVALID_TRIALS", "PERFORMANCE","R_PERF","L_PERF","PSYCH_X","PSYCH_Y","PSYCH_ERR","PSYCH_FIT", "MEAN_TIME","DATE"))
    # And append current session data:
    writer.writerow(("1", str(trial_num), str(correct_trials), str(invalid_trials),
    str(round(np.mean(performance), 2)), str(total_R_performance), str(total_L_performance), 
    str(xdata), str(ydata), str(error), str(fit), str(response_time), day))

else:
    file = open(".session_data.csv", "r+", newline='')
    # Read past session data for the inter-session report:
    for i, line in enumerate(file):
        if i > 0:
            line = line.split(';')
            correct_trial_record.append(line[2])
            invalid_trial_record.append(line[3])
            performance_record.append(line[4])
            R_performance_record.append(line[5])
            L_performance_record.append(line[6])
            psych_x_record.append(line[7])
            psych_y_record.append(line[8])
            psych_err_record.append(line[9])
            psych_fit_record.append(line[10])
            mean_times_record.append(line[11])
            dates_record.append(line[12][:-2])

    # Append current session data:
    writer = csv.writer(file, delimiter = ';')

    writer.writerow(("1", str(trial_num), str(correct_trials), str(invalid_trials),
    str(round(np.mean(performance), 2)), str(total_R_performance), str(total_L_performance), 
    str(xdata), str(ydata), str(error), str(fit), str(response_time), day))

file.close()

correct_trial_record.append(str(correct_trials))
invalid_trial_record.append(str(invalid_trials))
performance_record.append(str(round(np.mean(performance), 2)))
R_performance_record.append(str(total_R_performance))
L_performance_record.append(str(total_L_performance))
psych_x_record.append(str(xdata))
psych_y_record.append(str(ydata))
psych_err_record.append(str(error))
psych_fit_record.append(str(fit))
mean_times_record.append(str(response_time))
dates_record.append(day)

logging.debug("Data written to hidden CSV file. Continuing...")

##############################
# Create inter-session report:
##############################

def to_number_converter(var):
    return list(map(float, var))
def list_conv(var):
    return [list(map(float, elem[1:-1].split(','))) for elem in var]

invalid_trials = to_number_converter(invalid_trial_record)
performance_record = to_number_converter(performance_record)
R_performance_record = to_number_converter(R_performance_record)
L_performance_record = to_number_converter(L_performance_record)
psych_x = list_conv(psych_x_record)
psych_y = list_conv(psych_y_record)
psych_err = list_conv(psych_err_record)
psych_fit = list_conv(psych_fit_record)
mean_times = to_number_converter(mean_times_record)

with PdfPages('inter_session_report.pdf') as pdf:

        number_of_sessions = len(performance_record)
        if number_of_sessions <= 30:
            x_limit = 32
        else:
            x_limit = number_of_sessions + 2

        ########################################
        # FIRST PAGE (accuracy plots)
        ########################################

        plt.figure(figsize=(8, 12))
        axes1 = plt.subplot(411)

        axes1.set_xlim([0,x_limit])
        axes1.set_ylim([0,1.1])
        axes1.set_yticks(list(np.arange(0,1.1, 0.1)))
        axes1.set_yticklabels(['0', '', '','','','50', '','','','','100'])
        axes1.grid(linestyle = 'dashed', alpha = 0.6)
        axes1.set_title("Session accuracy", fontsize = 11)
        axes1.plot(range(1,number_of_sessions+1), performance_record, marker = 'o', markersize=3, color = 'black')
        axes1.plot(range(1,number_of_sessions+1), R_performance_record, marker = 'o', markersize=3, color = 'magenta')
        axes1.plot(range(1,number_of_sessions+1), L_performance_record, marker = 'o', markersize=3, color = 'cyan')
        axes1.set_ylabel('Accuracy [%]')
        axes1.set_xlabel('Session')
        plt.text(0.02, 0.98, "Subject name: " + subject_name, fontsize=8, transform=plt.gcf().transFigure)
        legend_elements = [Line2D([0], [0],color='w', marker='o',markersize=5, markerfacecolor="white", markeredgecolor = "black", label='Total performance'),
        Line2D([0], [0], marker='o', color='w', markersize=5, markerfacecolor="white", markeredgecolor = "cyan", label='Left channel'),
        Line2D([0], [0], marker='o', color='w',markersize=5, markerfacecolor="white", markeredgecolor = "magenta", label='Right channel')]
        plt.legend(bbox_to_anchor=(0.95,0.735), loc="lower right", bbox_transform= plt.gcf().transFigure, handles=legend_elements, ncol=3, prop={'size': 8})
        
        axes2 = plt.subplot(412)

        axes2.set_xlim([0,x_limit])
        axes2.set_ylim([0,1.1])
        axes2.set_yticks(list(np.arange(0,1.1, 0.1)))
        axes2.set_yticklabels(['0', '', '','','','50', '','','','','100'])
        axes2.grid(linestyle = 'dashed', alpha = 0.6)
        axes2.set_title("Session accuracy for evidences 1 and -1", fontsize = 11)
        axes2.plot(range(1,number_of_sessions+1), [elem[-1] for elem in psych_y], marker = 'o', markersize=3, color = 'cyan')
        axes2.plot(range(1,number_of_sessions+1), [1-elem[1] for elem in psych_y], marker = 'o', markersize=3, color = 'magenta')
        axes2.set_ylabel('Accuracy [%]')
        axes2.set_xlabel('Session')

        axes3 = plt.subplot(413)

        axes3.set_xlim([0,x_limit])
        max_value = np.amax(mean_times)
        if max_value < 400:
            axes3.set_ylim([0,500])
        else:
            axes3.set_ylim([0,max_value+100])
        
        axes3.grid(linestyle = 'dashed', alpha = 0.6)
        axes3.set_title("Session mean response times", fontsize = 11)
        axes3.plot(range(1,number_of_sessions+1), mean_times, marker = 'o', markersize=3, color = 'black')
        axes3.set_ylabel('Response time [ms]')
        axes3.set_xlabel('Session')

        axes4 = plt.subplot(414)

        axes4.set_xlim([0,x_limit])
        axes4.set_ylim([0,np.amax(invalid_trials) + 10])
        axes4.grid(linestyle = 'dashed', alpha = 0.6)
        axes4.set_title("Invalid trials per session", fontsize = 11)
        axes4.plot(range(1,number_of_sessions+1), invalid_trials, marker = 'o', markersize=3, color = 'black')
        axes4.set_ylabel('Trials')
        axes4.set_xlabel('Session')

        plt.subplots_adjust(bottom=0.1, top = 0.95, hspace = 0.5)
       
        pdf.savefig()  # saves the current figure into a pdf page
        plt.close() 

        ########################################
        # SECOND PAGE (psychometric plots)
        ########################################

        fig = plt.figure(figsize=(10, 4*(1+int((number_of_sessions-1)/3))))
        for index, _ in enumerate(performance_record):
            plt.subplot((1+int((number_of_sessions-1)/3)),3, index+1)
            plt.plot(np.linspace(-1,1,30), psych_fit[index], linewidth=0.8, c = 'black')
            plt.errorbar(psych_x[index], psych_y[index], yerr=psych_err[index], fmt='ro', markersize = 3)
            plt.plot([0,0], [0, 1], 'k-', lw=1, linestyle=':')
            plt.plot([-1, 1], [0.5, 0.5], 'k-', lw=1, linestyle=':')
            plt.xlim([-1.05, 1.05])
            plt.ylim([-0.05,1.05])
            plt.xlabel('Evidence')
            plt.ylabel('R-prob')
            plt.title(' '.join((dates_record[index], '(Session', str(index+1) + ')')), fontsize =10)
        plt.tight_layout()
        pdf.savefig(fig)  # saves the current figure into a pdf page
        plt.close()             

logging.info("Daily report done.")
logging.debug("Intersession report created. Finishing...")    

from daily_reports import report
report.main(glob.glob("*.csv")[0])



