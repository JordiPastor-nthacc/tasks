# !/usr/bin/python3
# -*- coding: utf-8 -*-
"""
to do in this version: add interactive behavior when >85% perf (18 out of 20) [real deal: how to update vector choice ~ rachid's trend plugin working fine with it?]
after each trial perfwindow is updated (list containing 0s and 1s, 0=punish, 1)
"""
import user_settings as conf # to use vars: conf.VAR_0
from pybpodapi.bpod import Bpod
from pybpodapi.state_machine import StateMachine
# from pybpodapi.protocol import Bpod, StateMachine
from pybpodapi.bpod.hardware.events import EventName
from pybpodapi.bpod.hardware.output_channels import OutputChannel
#from pysettings import conf
import timeit, random, numpy as np, time, datetime
from toolsR import SoundR, VideoR # DataR not req anymore
import os
from scipy.signal import firwin, lfilter
import sounddevice as sd

# check whether DATAdir and VIIDEOdir already exist, else create them
#currSessionPath = os.path.expanduser('~/DATA_pybpod/'+conf.VAR_BOX+'/') # VAR_BOX should look like 'BPOD01'
currSessionVid = os.path.expanduser('~/VIDEO_pybpod/'+conf.VAR_BOX+'/')
currSessionName = 'test_'+datetime.datetime.now().strftime('%Y%m%d_%H%M') # in a future replace test for the name R33_pprotocolname_+ others | would be good that this matches session timestamp
#if not os.path.exists(os.path.expanduser(currSessionPath)):
#    os.makedirs(os.path.expanduser(currSessionPath))
if not os.path.exists(os.path.expanduser(currSessionVid)):
    os.makedirs(os.path.expanduser(currSessionVid))
# set data
#mydata = DataR(path=currSessionPath, name=currSessionName)
# set  video
nocam = False
try:
    cam = VideoR(int(conf.VAR_CAMIDX), path= currSessionVid) #width='default', height='default',
    camOK=False                      #  fps=60)#, fourcc_mjpg='default', showWindows=True)
    cam.play()
    if int(conf.VAR_REC)>0:
        cam.record()
except:
    print("could not open device. This may happen because either it's already in use or wrong device index number was provided")
    nocam = True

def getWaterCalib(box):
    '''returns tupple (L,C,R) valvetime for a given box (boardname inside .npy calib-file is req to match). Still dont know what happens if non-existen str is used as arg'''
    watervalues = np.load(os.path.expanduser('~/pluginsr-for-pybpod/water-calibration-plugin/DATA/water_calibration_hystory.npy')).item()
    idx = next(i for i,v in zip(range(len(list(watervalues['board']))-1, -1, -1), reversed(list(watervalues['board']))) if v == box)
    return watervalues['results'][0][idx], watervalues['results'][1][idx], watervalues['results'][2][idx]

def getBroadbandCalib(xonaridx):
    '''returns tupple (L,R) of amp [latter calib values] of a given soundcard index (same as SoundR.getDevices())'''
    broadband = np.load(os.path.expanduser('~/pluginsr-for-pybpod/sound-calibration-plugin/DATA/broadbandnoise_hystory.npy')).item()
    Lidx = next(i for i,v,w in zip(range(len(list(broadband['Bsoundcard']))-1, -1, -1), reversed(list(broadband['Bsoundcard'])), reversed(list(broadband['Bside']))) if v.startswith(str(xonaridx)) and w=='L')
    Ridx = next(i for i,v,w in zip(range(len(list(broadband['Bsoundcard']))-1, -1, -1), reversed(list(broadband['Bsoundcard'])), reversed(list(broadband['Bside']))) if v.startswith(str(xonaridx)) and w=='R')
    return (broadband['BAmp'][Lidx], broadband['BAmp'][Ridx])

try:# find last water calibration
    ValveLtime, ValveCtime, ValveRtime = getWaterCalib(conf.VAR_BOX)
    print('valve timings (L, R):')
    print(ValveLtime, ValveRtime)
except:
    print('did not find water calib file. loading [0.08]*3')
    ValveLtime, ValveCtime, ValveRtime = (0.12, 0.12, 0.12)

try:# same but broadband
    LAmp, RAmp = getBroadbandCalib(int(conf.VAR_XONAR))
    print('Amp values (L,R):'+str(LAmp)+' '+str(RAmp))
except:
    print('did not find broadband calib file. loading amplitudes (0.5, 0.5)')
    LAmp, RAmp = (0.5, 0.5)
print(LAmp, RAmp)
# session parameters [need to group them altogether] at the start of the script, when possible
LEDINT = 8 # from 0 to 255
LEDL, LEDC, LEDR = (OutputChannel.PWM1,LEDINT), (OutputChannel.PWM2,LEDINT), (OutputChannel.PWM3,LEDINT)
nTrials = 3000
sessionTimeLength = 5400


coin = random.choice(['heads', 'tails']) # from which side should be the first block
coin = 'tails'
L = [0]*50
R = [1]*50
if coin == 'heads':
    trial_list = (L+R)*1
    #trial_list = [0]*15 # testing purposes since we cannot stop the protocol once it starts. Original statement right above.
else:
    trial_list = (R+L)*1
    #trial_list = [1]*15 #same here
trial_list = []
# testing
#trial_list = [0, 1]*100
pending_trials = np.random.choice([0,1], nTrials - len(trial_list)).tolist()
trial_list = trial_list + pending_trials



#trialTypes = [0, 1]  # 1 (rewarded left) or 2 (rewarded right)
# managing the no-stop button issue

sessionTimeStart = time.time()
sessionTimeEnding = sessionTimeStart+sessionTimeLength

# Generation of white noise.
mean = 0
std = 1
duration = 33
#amplitude = 0.5 # need to import calib and do it for both sides | by hand till new version // creating 2 new vars: VAR_AMPL and VAR_AMPL
FsOut = 44100  # sample rate, depend on the sound card
band_fs = [1000, 20000]
white_noise1 = LAmp * np.random.normal(mean, std, size=FsOut * (duration + 1)) # 1 second more that will be removed at the end
# Filter the white noise using a pass-band
Fn = 10000 # Lenght of the filter
band_pass = firwin(Fn, [band_fs[0]/(FsOut*0.5), band_fs[1]/(FsOut*0.5)], pass_zero=False) # Pass-band fileter
band_noise1 = lfilter(band_pass, 1, white_noise1) # Filtering
s1 = band_noise1[FsOut:FsOut*(duration+1)] # remove the first second: it has some zeros casused by the phase of the filter LEFT
s01 = np.zeros(s1.size) # Empty vector of the same size.

white_noise2 = RAmp * np.random.normal(mean, std, size=FsOut * (duration + 1))
band_noise2 = lfilter(band_pass, 1, white_noise2)
s2 = band_noise2[FsOut:FsOut*(duration+1)]
s02 = np.zeros(s2.size)

perfwindow = [0]*20
# Create the sound server 
soundStream = SoundR(sampleRate=FsOut, deviceOut=int(conf.VAR_XONAR), channelsOut=2) #change device depending on the sound card

# Code to trig the sound

def my_softcode_handler(data):
    global start, timeit, soundStream, cam
    print(data)
    if data == 68:
        print("Play")
        soundStream.playSound()
        start = timeit.default_timer()
    elif data == 66:
        soundStream.stopSound()
        print(':::::::::Time to stop:::::::::', timeit.default_timer() - start)
        

START_APP = timeit.default_timer()

# we will define a function so it gets evaluated in each trial. will it? :)
def remainingTime():
    return sessionTimeEnding-time.time()

my_bpod = Bpod() 
my_bpod.register_value('VECTOR_CHOICE', trial_list) # required for trend plugin [0-left, 1-right]
# here, try to include this statement inside the loop so we can gain interactivity (if it does not lag :D)
my_bpod.softcode_handler_function = my_softcode_handler

while True:
    for trial in trial_list:  # Main loop
        print(trial)

        if trial == 0:
            valvetime = ValveLtime
            rewardValve = (OutputChannel.Valve, 1)
            soundStream.load(s1, s01)
            pokechange = {EventName.Port1In:'Reward', EventName.Port3In:'Punish'}#EventName.Tup:  goIdle}
            rewardLED = LEDL
        elif trial == 1:
            valvetime = ValveRtime
            rewardValve = (OutputChannel.Valve, 3)
            soundStream.load(s02, s2)
            pokechange = {EventName.Port3In:'Reward', EventName.Port1In:'Punish'}#EventName.Tup: goIdle}
            rewardLED = LEDR

        sma = StateMachine(my_bpod)
        sma.set_global_timer_legacy(timer_id=1, timer_duration=remainingTime())
        
        sma.add_state(
            state_name='WaitCPoke',
            state_timer=1,
            state_change_conditions={Bpod.Events.Port2In:'Fixation', Bpod.Events.GlobalTimer1_End: 'exit'},
            output_actions=[LEDC,(Bpod.OutputChannels.GlobalTimerTrig, 1)])
        sma.add_state(
            state_name='Fixation',
            state_timer=0.3,
            state_change_conditions={Bpod.Events.Port2Out:'WaitCPoke', EventName.Tup:'StartSound'},
            output_actions=[LEDC])
        sma.add_state(
            state_name='StartSound',
            state_timer=0.2,
            state_change_conditions=pokechange,
            output_actions=[(OutputChannel.SoftCode, 68)])#,(Bpod.OutputChannels.GlobalTimerTrig, 1)])
        sma.add_state(
            state_name='Reward',
            state_timer=valvetime,
            state_change_conditions={EventName.Tup: 'keep-led-on'},
            output_actions=[(OutputChannel.SoftCode, 66), rewardValve, rewardLED])
        sma.add_state(
            state_name='keep-led-on',
            state_timer=0.3-valvetime,
            state_change_conditions= {EventName.Tup: 'exit'},
            output_actions=[rewardLED])
        sma.add_state(
            state_name='Punish',
            state_timer=2,
            state_change_conditions= {EventName.Tup: 'exit'},
            output_actions=[(OutputChannel.SoftCode, 66)])

        my_bpod.send_state_machine(sma)  # Send state machine description to Bpod device

        print("Waiting for poke. Reward: ", 'left' if trial == 0 else 'right')

        my_bpod.run_state_machine(sma)  # Run state machine

        trialstring = ("Current trial info: {0}".format(my_bpod.session.current_trial))
        #print(my_bpod.session.current_trial) # testing, delete
        #print(type(my_bpod.session.current_trial.export())) # works, consider using this nstead of crappy strings || do some timeit runs
        #print((dict(my_bpod.session.current_trial))['idle_stop_sound'])#testing, delete
        #if 'idle_stop_sound' in list(my_bpod.session.current_trial.keys()): # if true means there is no timestamp on it because this state was not triggered
        #    counter_idle_stop_sound += 1
             # we reset the counter because the rat poked in the correct port in this trial.
        #else:
        #    counter_idle_stop_sound = 0
        #if my_bpod.session.current_trial['idle_stop_sound'] == [(np.nan, np.nan)]:
        # since i had many issues i created a new state to exit idle namely exitIdle (because if trying to leave with punish, it makes a closed loop p)

        if "'Punish': [(nan, nan)]" in trialstring:# Punish state was not triggered = rat poked in the correct port. This one is enough since it is binary
            perfwindow = perfwindow[1:]+[1]
        else:
            perfwindow = perfwindow[1:]+[0]
        #perf = sum(perfwindow)/2 #sum should be faster than for loop, nonetheless np is way faster
        # nonsense to calculate, with len=20 window, >85% means 18 x 1 out of 20 (or even more)
        if sum(perfwindow) > 17:
            print('performance window is over 85%')

        ########################################################
        # still need to do something similar but with performance window, so it goes full random or not
        ########################################################

        if time.time()>sessionTimeEnding:
            break
    if time.time()>sessionTimeEnding:
        break
my_bpod.close()  # Disconnect Bpod and perform post-run actions

#if nocam==False:
#    cam.stop()
cam.stop()
#mydata.save()
print('EXECUTION TIME', timeit.default_timer() - START_APP)
#print(currSessionName)
#print(currSessionVid)